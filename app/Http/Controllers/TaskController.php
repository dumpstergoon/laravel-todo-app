<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Http\Requests\TaskStoreRequest;
use Illuminate\Http\Request;


class TaskController extends Controller
{
	// TODO: move these elsewhere?
	private function get_selected($a, $b) {
		return $a == $b ? 'selected' : '';
	}

	private function get_enabled($mode, $state, $value) {
		return $mode != 'create' && $state == $value ? 'enabled' : 'disabled';
	}

	// USED BY: GET @ /create, GET @ /{id}, GET @ /{id}/edit
	/**
	 * 'Oor custom view. CREATE(), SHOW(), and EDIT() use this.
	*/
	private function view(Task $task, $mode) {
		$state = $task->state;
		$priority = $task->priority ?? 'normal';
		$parent_id = $task->task_id ?? '';
		$parent_url = $parent_id ? '/tasks/'.$parent_id.'/' : '/';
		
		return view('task-details', $task)
					->with('mode', $mode)
					->with('task_id', $parent_id)
					->with('parent_url', $parent_url)
					->with('method', $mode == 'edit' ? 'PATCH' : 'POST')
					->with('action', $mode != 'create' ?
										$parent_url.'tasks/'.$task->id : $parent_url)
					->with('action_state', $mode != 'view' ? 'enabled' : 'disabled')
					->with('state_btns', [
						'complete' => $this->get_enabled($mode, $state, 'open'),
						'open' => $this->get_enabled($mode, $state, 'closed'),
					])
					->with('priorities', [
						'low' => $this->get_selected($priority, 'low'),
						'normal' => $this->get_selected($priority, 'normal'),
						'high' => $this->get_selected($priority, 'high'),
						'urgent' => $this->get_selected($priority, 'urgent'),
					]);
	}

	// GET @ /
	/**
	 * Display a listing of the resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request)
	{
		$data = [
			'pending' => Task::where('state', '=', 'open')
							//->orderBy('order')
							->oldest()
							->get(),
			'completed' => Task::where('state', '=', 'closed')
							//->orderBy('order')
							->oldest()
							->get()
		];

		if ($request->wantsJson())
			return $data;
		
		return view('tasks-overview', $data);
	}

	// GET @ /create
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return $this->view(Task::make(), 'create');
	}

	// POST @ /
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(TaskStoreRequest $request)
	{
		// TODO: build TaskStoreRequest
		// for validation stuffs.
		$task = Task::make($request->toArray());
		$task->tags = $task->tags ?? '';
		$task->save();
		
		if ($request->wantsJson())
			return $task;
		
		return $this->index($request);
	}

	// GET @ /{id}
	/**
	 * Display the specified resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, Task $task)
	{
		if ($request->wantsJson())
			return $task;
		return $this->view($task, $task->state == 'closed' ? 'view' : 'edit');
	}

	// GET @ /{id}/edit
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Models\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Task $task)
	{
		return $this->view($task, 'edit');
	}

	// PUT/PATCH @ /{id}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Models\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	// TODO: update Request to TaskUpdateRequest...
	public function update(Request $request, Task $task)
	{
		$task->fill($request->toArray());
		$task->tags = $request->tags ?? '';
		$task->save();
		return $this->show($request, $task);
	}

	// DELETE @ /{id}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Models\Task  $task
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Could we change the above parameter to
		// be just "$id" and then we can call
		Task::destroy($id);
		// $task->delete();
		// This is not good... what should we do here?
		return ['status' => 'OK'];
	}
}
