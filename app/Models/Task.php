<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'note',
        'tags',
        'priority',
        'order',
        'state',
        'task_id'
    ];

    protected $attributes = [
        'note' => '',
        'tags' => '',
        'priority' => 'normal',
        'order' => 0,
        'state' => 'open',
    ];
}
