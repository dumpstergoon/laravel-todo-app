<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            
            // Allow tasks to have other tasks as a parent.
            $table->foreignId('task_id')
                ->nullable(true);
            $table->index('task_id');

            // Equivalent to "unfinished"/"finished"
            $table
                ->enum('state', ['open', 'closed'])
                    ->default('open');
            $table->index('state');
            
            // Tasks should have a defined priority set
            $table
                ->enum('priority', ['low', 'normal', 'high', 'urgent'])
                    ->default('normal');
            $table->index('priority');
            
            // Tasks don't really have names nor descriptions but kinda both in one:
            $table->text('note')->nullable(false);

            /*
                TODO: a "due" column is something to consider...
                but it might be better served as a *separate
                table* called "alarms" or "reminders" such
                that extra properties can be set (recurring, etc.)
            */
            //$table->dateTime('due', 0);

            // NOTE: Switch to JSON array?
            $table->string('tags', 100)->default('');

            // The user *might* order their tasks despite of creation/due date.
            // default is -1 (new items always on top, baby)
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
