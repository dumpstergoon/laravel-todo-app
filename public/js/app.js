// Let's keep'er simple.
class API {
	endpoint = '/';

	constructor(base_uri = '/') {
		this.endpoint = base_uri.endsWith('/') ? base_uri : base_uri + '/';
	}

	req(uri, method, data, callback) {
		uri = new String(uri).startsWith('/') ? uri.substr(1) : uri + '';
		let opts = {
			method,
			// *default, no-cache, reload, force-cache, only-if-cached
			cache: 'no-cache',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'X-Requested-With': 'XMLHttpRequest',
				// 'Content-Type': 'application/x-www-form-urlencoded',
			}
		};
		if (method !== 'GET')
			opts.body = JSON.stringify(data);
		return fetch(this.endpoint + uri, opts)
				.then(resp => resp.json()).then(callback);
	}
	
	get(uri, data, callback) {
		if (!callback) {
			callback = data;
			data = {};
		}
		return this.req(uri, 'GET', data, callback);
	}
	
	post(uri, data, callback) {
		return this.req(uri, 'POST', data, callback);
	}
	
	update(uri, data, callback) {
		return this.req(uri, 'PATCH', data, callback);
	}
	
	destroy(uri, callback) {
		return this.req(uri, 'DELETE', {}, callback);
	}
}

const api = new API('/tasks');
const buttons = Object.fromEntries(
				Array.from(document.getElementsByTagName('button'))
					 .map(button => [button.id.split('_btn')[0], button]));