@extends('layouts.app')
@section('title', ucwords($mode) ?? 'Mystery Mode')

@section('content')
	<button id="back_btn" class="back">&#10229;</button>
	<header>
		<h1>
			{{ ucwords($mode) }}
			{{ $mode != 'create' ? 'Task (id#'.$id.')' : 'Task' }}
		</h1>
		<button
			id="delete_btn"
			class="delete"
			title="Delete Task"
			{{ $mode == 'create' ? 'disabled' : 'enabled' }}>&#9760;</button>
	</header>

	<form id="task_form" method="POST" action="{{ $action }}">
		@method($method)
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div>
			<label class="status">Status</label>
			<span class="status {{ $state }}">{{ $state }}</span>
			<button
				id="complete_btn"
				type="button"
				class="save"
				{{ $state_btns['complete'] }}>Mark as Completed</button>
			<button
				id="open_btn"
				type="button"
				class="edit"
				{{ $state_btns['open'] }}>Mark as Open</button>
		</div>
		<div>
			<label>Priority</label>
			<select name="priority" {{ $action_state }}>
				<option value="low" {{ $priorities['low'] }}>low</option>
				<option value="normal" {{ $priorities['normal'] }}>normal</option>
				<option value="high" {{ $priorities['high'] }}>high</option>
				<option value="urgent" {{ $priorities['urgent'] }}>urgent</option>
			</select>
		</div>
		<div>
			<label class="required">Note</label>
			<textarea
				name="note"
				placeholder="What's the deal?"
				{{ $action_state }}>{{ $note ?? '' }}</textarea>
		</div>
		<div>
			<label>Tags</label>
			<input
				name="tags"
				type="text"
				placeholder="work,javascript,etc..."
				value="{{ $tags ?? '' }}"
				{{ $action_state }}/>
		</div>
		<div class="actions">
			<button
				id="save_btn"
				type="submit"
				class="save"
				{{ $action_state }}>Save</button>
			<button
				id="cancel_btn"
				type="button"
				class="cancel"
				{{ $action_state }}>Cancel</button>
		</div>
	</form>
@endsection

@section('scripts')
	<script type="module">
		const id = {{ $id ?? -1 }};
		const mode = "{{ $mode }}";
		const parent_url = "{{ $parent_url }}";

		const form = document.getElementById('task_form');

		function exit(e) {
			window.location.href = parent_url;
			if (e.preventDefault)
				e.preventDefault();
		}

		[buttons.back, buttons.cancel]
			.forEach(el => el.addEventListener('click', exit));
		
		buttons.complete.addEventListener('click', e => {
			api.update(id, {
				state: 'closed'
			}, exit);
		});

		buttons.open.addEventListener('click', e => {
			api.update(id, {
				state: 'open'
			}, exit);
		});

		buttons.delete.addEventListener('click', e => {
			if (confirm('⚠️ You sure, bruh?'))
				api.destroy(id, exit);
		});

		if (mode === "edit") {
			// Override save button such that it handles the update
			// and navigates to the parent page instead of refreshing
			// the same page.
			buttons.save.addEventListener('click', e => {
				const data = Object.fromEntries(new FormData(form).entries());
				api.update(id, data, exit);
				e.preventDefault();
			})
		}

	</script>
@endsection