@extends('layouts.app')
@section('title', 'Dashboard')

@section('content')
	<h1>Hello, TODO App:</h1>

	@if (count($pending) > 0)
		<ol class="tasks pending">
			@foreach ($pending as $task)
				<li class="{{ $task->priority }}">
					<a href="/tasks/{{ $task->id }}">{{ $task->note }}</a>
					@if ($task->tags)
						<br /><span class="tags">{{ $task->tags }}</span>
					@endif
				</li>
			@endforeach
		</ol>
	@else
		<p>
			That's all, folks!
		</p>
	@endif

	<div class="actions">
		<button id="add_task_btn" class="add">Add Task</button>
	</div>

	@if (count($completed) > 0)
		<hr />
		<h2>Completed Tasks:</h2>
		<ul class="tasks completed">
			@foreach ($completed as $task)
				<li>
					<a href="/tasks/{{ $task->id }}">{{ $task->note }}</a><br />
				</li>
			@endforeach
		</ul>
	@endif
@endsection

@section('scripts')
	<script type="module">
		buttons.add_task.addEventListener(
			'click', e => window.location.href = '/create'
		);
	</script>
@endsection